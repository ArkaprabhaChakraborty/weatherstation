from flask import Flask, redirect, render_template, request, flash, url_for

from user_collection_handler import add_user_data, validate_user, fetch, fetch_all, delete_user, fetch_sensor_data_all
from realtime_data_handler import upload_realtime_data, get_realtime_data, archive_realtime_data

app = Flask(__name__,template_folder='templates')

@app.route('/')
def home():
    password_param = request.args.get('password_param')
    return render_template('login.html', password_param=password_param)

@app.route('/loginval', methods= ['GET','POST'])
def login_val():
    email = request.form.get('email')
    password = request.form.get('password')
    if email is not None and password is not None:
        if(validate_user(email, password)):
            return redirect('/overview')
        else:
            return redirect(url_for('.home',password_param=True))
    else:
        return render_template(url_for('.home',password_param=False))
    

@app.route('/overview', methods= ['GET','POST'])
def overview():
    return render_template('mainbody.html',param='overview.html')

@app.route('/maintainence', methods= ['GET','POST'])
def management():
    data = fetch_sensor_data_all()
    return render_template('mainbody.html',param="maintainence.html",data = data) 

@app.route('/archive', methods= ['GET','POST'])
def archive():
    data = get_realtime_data()
    timestamps = [x for x in data.keys()]
    return render_template('mainbody.html',param="archive.html",timestamps=timestamps,data=data)

@app.route('/archivingdata', methods= ['GET','POST'])
def archivingdata():
    print(archive_realtime_data())
    return redirect(url_for('.archive'))


@app.route('/register', methods= ['GET','POST'])
def register():
    email = request.form.get('email')
    password = request.form.get('password')
    name = request.form.get('name')
    contact = request.form.get('contact')
    data = dict()
    if email is not None and password is not None and name is not None and contact is not None:
        data['email'] = email
        data['password'] = password
        data['name'] = name
        data['phone'] = contact
        print(add_user_data(data))
    return redirect(url_for('.accessmanagement'))


@app.route('/accessmanagement', methods= ['GET','POST'])
def accessmanagement():
    users = fetch_all()
    return render_template('mainbody.html',param="accessmanagement.html",users = users)


if __name__ == '__main__':
    app.run(debug=True)