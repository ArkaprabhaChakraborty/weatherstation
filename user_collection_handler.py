import time
import firebase_admin
from firebase_admin import firestore
from firebase_admin import credentials
from datetime import datetime
import random
import json

from realtime_data_handler import db_firestore

def add_user_data(data):
    doc_ref = db_firestore.collection('users').document(data['email'])
    doc_ref.set({
        'email': data['email'], 
        'password': data['password'], 
        'phone': data['phone'], 
        'name': data['name']
    })
    return "Successfully added user"

def add_sensor_data(data):
    doc_ref = db_firestore.collection('sensors').document(data['hardware_id'])
    doc_ref.set({
        'hardware': data['hardware'],
        'status': data['status'],
        'health': data['health'],
        'timestamp': data['timestamp']
    })

def fetch_sensor_data_all():
    doc_ref = db_firestore.collection('sensors').get()
    sensors_list = []
    for doc in doc_ref:
        doc_ref = doc.to_dict()
        sensors_list.append(doc_ref)
    return sensors_list

def validate_user(email, password):
    doc_ref = db_firestore.collection('users').document(email).get()
    if password == doc_ref.get('password'):
        print("Successfully validated user")
        return True
    else:
        return False

def fetch(email):
    doc_ref = db_firestore.collection('users').document(email).get()
    doc_ref = doc_ref.to_dict()
    doc_ref.pop('password')
    return doc_ref

def fetch_all():
    doc_ref = db_firestore.collection('users').get()
    users_list = []
    for doc in doc_ref:
        doc_ref = doc.to_dict()
        doc_ref.pop('password')
        users_list.append(doc_ref)
    return users_list

def delete_user(email):
    doc_ref = db_firestore.collection('users').document(email).delete()
