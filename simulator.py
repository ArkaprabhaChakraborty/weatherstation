import time
import random
from user_collection_handler import add_sensor_data, fetch_sensor_data_all

sensornames = ['thermomter', 'humidity-sensor', 'co2', 'barometer', 'anemomter', 'CPU', 'battery']
statuses = ['normal', 'warning', 'critical']
healths = ['good', 'bad']

for i in range(len(sensornames)):
    sensor_data = dict()
    sensor_data['hardware_id'] = str(i)
    sensor_data['hardware'] = sensornames[i]
    sensor_data['status'] = statuses[random.randint(0,len(statuses)-1)]
    sensor_data['health'] = healths[random.randint(0,len(healths)-1)]
    sensor_data['timestamp'] = int(time.time() * 10000000)
    add_sensor_data(sensor_data)

print(fetch_sensor_data_all())