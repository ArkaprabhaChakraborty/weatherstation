import firebase_admin
from firebase_admin import db
from firebase_admin import credentials
import time
import random
import json
from firebase_admin import firestore

cred = credentials.Certificate("weatherstation-61846-firebase-adminsdk-g13vg-2d4aa3f933.json")
db_app = firebase_admin.initialize_app(cred, {
    'databaseURL': 'https://weatherstation-61846-default-rtdb.firebaseio.com/'
})
db_firestore = firestore.client()

def upload_realtime_data():
  for j in range(10):
    timestamp = time.time()
    ref = db.reference('Weather Data')
    ref.child(str(int(timestamp * 10000000))).child('temperature').set(str(random.random()*100))
    ref.child(str(int(timestamp * 10000000))).child('relative_humidity').set(str(random.random()*100))
    ref.child(str(int(timestamp * 10000000))).child('absolute_humidity').set(str(random.random()*100))
    ref.child(str(int(timestamp * 10000000))).child('pressure').set(str(random.random()*100))
    ref.child(str(int(timestamp * 10000000))).child('windspeed').set(str(random.random()*100))
    ref.child(str(int(timestamp * 10000000))).child('winddirection').set("NE")
    ref.child(str(int(timestamp * 10000000))).child('precipitation').set(str(random.random()*100))
    ref.child(str(int(timestamp * 10000000))).child('dewpoint').set(str(random.random()*100))
    ref.child(str(int(timestamp * 10000000))).child('battery').set(str(random.random()*100))
    ref.child(str(int(timestamp * 10000000))).child('overall_weather').set(str(random.random()*100))

def get_realtime_data():
  ref = db.reference('Weather Data')
  ref = ref.get()
  return ref 

def archive_realtime_data():
  ref = db.reference('Weather Data')
  ref = ref.get()
  archive_id = "ARCHIVED_AT" + str(int(time.time() * 10000000))
  doc_ref = db_firestore.collection('weather_data').document(archive_id)
  doc_ref.set(ref)
  return "Successfully Archived"

  